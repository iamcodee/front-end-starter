import FormValidation from '../classes/FormValidation'

const $forms = document.querySelectorAll('.js-form-validation')

const cfg = {
  classTo: 'f-field',
  errorClass: 'f-field--has-error',
  successClass: 'f-field--has-success',
  errorTextParent: 'f-field',
  errorTextTag: 'span',
  errorTextClass: 'f-error'
}

const formValidations = []

$forms.forEach($form => {
  const formValidation = new FormValidation($form, cfg)
  formValidation.init()
  formValidations.push(formValidation)
})
