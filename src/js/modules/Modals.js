import Modal from '../classes/Modal'

const $modals = document.querySelectorAll('.js-modal')
export const modals = []

// standard modals
$modals.forEach($modal => {
  const modal = new Modal($modal)
  modal.init()
  modals.push(modal)
})
