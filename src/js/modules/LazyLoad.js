import lazyload from 'vanilla-lazyload'

const myLazyLoad = new lazyload({
  elements_selector: '.js-lazy',
  threshold: 0
})
