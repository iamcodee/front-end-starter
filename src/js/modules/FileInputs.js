import FileInput from '../classes/FileInput'

const $fileInputs = document.querySelectorAll('.js-file')
const fileInputs = []

$fileInputs.forEach($fileInput => {
  const fileInput = new FileInput($fileInput)
  fileInput.init()
  fileInputs.push(fileInput)
})
