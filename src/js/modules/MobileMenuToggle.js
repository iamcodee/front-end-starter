import MobileMenuToggle from '../classes/MobileMenuToggle'

const $menu = document.querySelector('.js-menu')
const $btn = document.querySelector('.js-menu-toggle')
const mobileMenuToggle = new MobileMenuToggle({
  menu: $menu,
  btn: $btn
})
mobileMenuToggle.init()
