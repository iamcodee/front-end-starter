import CookieNotice from '../classes/CookieNotice'

const $cookieNoticeBox = document.querySelector('.js-cookie-notice')
const cookieNotice = new CookieNotice($cookieNoticeBox)
cookieNotice.init()
