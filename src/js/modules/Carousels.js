import Carousel from '../classes/Carousel'

const $carousel = document.querySelector('.js-carousel')
const carousel = new Carousel($carousel, {
  tns: {
    items: 4,
    gutter: 10,
    nav: true
  }
})
carousel.init()
