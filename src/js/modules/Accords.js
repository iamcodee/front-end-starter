import Accord from '../classes/Accord'
import AccordGroup from '../classes/AccordGroup'

// single default accords not in group
const $accords = document.querySelectorAll('.js-accord')
const accords = []

$accords.forEach($accord => {
  const accord = new Accord($accord)
  accord.init()
  accords.push(accord)
})

// default groups of accords
const $accordGroups = document.querySelectorAll('.js-accord-group')
const accordGroups = []

$accordGroups.forEach($accordGroup => {
  const $group = $accordGroup.querySelectorAll('.js-accord-group-item')
  const accordGroup = new AccordGroup($group)
  accordGroup.init()
  accordGroups.push(accordGroup)
})
