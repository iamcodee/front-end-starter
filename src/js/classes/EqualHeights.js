import throttle from 'lodash/throttle'

export default class {
  constructor (items) {
    this.items = items
    this.groups = [[]]
  }

  overlapsY (a, b) {
    const rect1 = a.getBoundingClientRect()
    const rect2 = b.getBoundingClientRect()
    return rect1.top <= rect2.bottom && rect2.top <= rect1.bottom
  }

  groupElements () {
    this.groups = [[]]

    for (let index = 0; index < this.items.length - 1; index++) {
      const itemA = this.items[index]
      const itemB = this.items[index + 1]

      if (index === 0) this.groups[0].push(itemA)

      if (this.overlapsY(itemA, itemB)) {
        const lastGroupIndex = this.groups.length - 1
        this.groups[lastGroupIndex].push(itemB)
      } else {
        this.groups.push([itemB])
      }
    }
  }

  resizeHeights () {
    this.groups.forEach(group => {
      let minHeight = 0
      group.forEach(item => {
        item.style.minHeight = '0px'
        if (item.offsetHeight > minHeight) minHeight = item.offsetHeight
      })

      group.forEach(item => {
        item.style.minHeight = `${minHeight}px`
      })
    })
  }

  makeEqualHeights () {
    this.groupElements()
    this.resizeHeights()
  }

  initResize () {
    window.addEventListener(
      'resize',
      throttle(this.makeEqualHeights.bind(this), 200))
  }

  init () {
    this.makeEqualHeights()

    this.initResize()
  }
}