import { slideUp, slideDown } from '../utilities'

export default class {
  constructor (ctx, cfg) {
    this.ctx = ctx

    if (!this.ctx) {
      throw new Error('Accord context not specified')
    }

    this.initCfg(cfg)

    this.trigger = this.ctx.querySelector(this.settings.triggerSelector)
    this.content = this.ctx.querySelector(this.settings.contentSelector)
    this.active = false
  }

  initCfg (cfg) {
    this.settings = {
      triggerSelector: '.js-accord-trigger',
      contentSelector: '.js-accord-content',
      activeClass: 'is-active',
      ...cfg
    }
  }

  slideDown () {
    this.active = true
    this.ctx.classList.add(this.settings.activeClass)

    slideDown(this.content)
  }

  slideUpOthers () {
    this.group.forEach(item => {
      if (item.active) item.slideUp()
    })
  }

  slideUp () {
    this.active = false
    this.ctx.classList.remove(this.settings.activeClass)

    slideUp(this.content)
  }

  toggleItem () {
    if (!this.ctx) return

    if (this.active) {
      this.slideUp()
    } else {
      if (this.group) this.slideUpOthers()
      this.slideDown()
    }
  }

  initItem () {
    if (this.trigger) {
      this.trigger.addEventListener('click', () => {
        this.toggleItem()
      })
    }
  }

  init (group) {
    if (group) this.group = group
    this.initItem()
  }
}
