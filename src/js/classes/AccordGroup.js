import Accord from './Accord'

export default class {
  constructor (group) {
    this.group = group
  }

  init () {
    this.accords = Array.from(this.group).map(item => new Accord(item))

    this.accords.forEach(item => {
      item.init(this.accords)
    })
  }
}
