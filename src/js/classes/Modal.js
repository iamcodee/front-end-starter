export default class {
  constructor (ctx, cfg) {
    this.ctx = ctx

    if (!this.ctx) {
      throw new Error('Modal context not specified')
    }

    this.initConfig(cfg)

    this.btn = this.ctx.querySelector(this.settings.closeBtnSelector)
  }

  initConfig (cfg) {
    this.settings = {
      contentClass: 'js-modal-container',
      closeBtnSelector: '.js-modal-close',
      hiddenClass: 'u-hidden',
      ...cfg
    }
  }

  initBtn () {
    if (!this.btn) {
      throw new Error('Modal close button not specified')
    }

    this.btn.addEventListener('click', () => {
      this.ctx.classList.add(this.settings.hiddenClass)
    })
  }

  checkIfClikedOutside (event) {
    const classes = event.path.map(item => item.className).join(' ')

    return !classes.includes(this.settings.contentClass)
  }

  initWindowClick () {
    window.addEventListener('click', e => {
      if (this.checkIfClikedOutside(e)) {
        this.ctx.classList.add(this.settings.hiddenClass)
      }
    })
  }

  init () {
    this.initBtn()
    this.initWindowClick()
  }
}
