export default class {
  constructor (ctx, cfg) {
    this.ctx = ctx

    if (!this.ctx) {
      throw new Error('FileInput context not specified')
    }

    this.initCfg(cfg)

    this.input = this.ctx.querySelector(this.settings.inputSelector)
    this.label = this.ctx.querySelector(this.settings.labelSelector)

    if (!this.input) {
      throw new Error('FileInput`s input element not specified')
    }

    if (!this.label) {
      throw new Error('FileInput`s label element not specified')
    }

    this.labelText = this.label.innerHTML
  }

  initCfg (cfg) {
    const dataset = this.ctx.dataset
    // get settings from datasets
    const hasCorrectSizeLimit = dataset?.sizeLimit && Number.parseInt(dataset.sizeLimit)
    const hasCorrectCountLimit = dataset?.countLimit && Number.parseInt(dataset.countLimit)

    this.datasets = {}

    if (hasCorrectSizeLimit) {
      this.datasets.sizeLimit = Number.parseInt(dataset.sizeLimit)
    }

    if (hasCorrectCountLimit) {
      this.datasets.countLimit = Number.parseInt(dataset.countLimit)
    }

    // combine config by order datset > cfg > default
    this.settings = {
      inputSelector: '.js-file-input',
      labelSelector: '.js-file-label',
      sizeLimit: false,
      countLimit: false,
      ...cfg,
      ...this.datasets
    }

    this.sizeLimit = this.settings.sizeLimit *= (1024 * 1024)
  }

  checkSize (event) {
    if (!this.settings.sizeLimit) return true

    const size =
      Array.from(event.target.files)
        .reduce(
          (prev, file) => prev + file.size,
          0
        )

    return size <= this.settings.sizeLimit
  }

  checkCount (event) {
    if (!this.settings.countLimit) return true
    const count = event.target.files.length

    return count <= this.settings.countLimit
  }

  getLabelText (event) {
    return Array.from(event.target.files).map(file => file.name).join(', ')
  }

  initInput () {
    this.input.addEventListener('change', e => {
      if (!this.checkSize(e) || !this.checkCount(e)) return

      this.label.innerHTML = this.getLabelText(e)
    })
  }

  init () {
    this.initInput()
  }
}
