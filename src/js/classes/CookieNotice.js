import cookies from 'cookie.js'

export default class {
  constructor (ctx, cfg) {
    this.ctx = ctx

    if (!this.ctx) {
      throw new Error('Cookie notice context not specified')
    }

    this.initCfg(cfg)

    this.btn = this.ctx.querySelector(this.settings.cookieBtnSelector)
  }

  initCfg (cfg) {
    this.settings = {
      cookieName: 'cookie-notice',
      hiddenClass: 'u-hidden',
      cookieBtnSelector: '.js-cookie-notice-close',
      ...cfg
    }
  }

  initBtn () {
    if (!this.btn) return

    this.btn.addEventListener('click', () => {
      cookies.set(this.settings.cookieName, 'true')
      this.ctx.classList.add(this.settings.hiddenClass)
    })
  }

  checkIfAccepted () {
    return cookies.get(this.settings.cookieName)
  }

  init () {
    if (!this.checkIfAccepted()) {
      this.ctx.classList.remove(this.settings.hiddenClass)
      this.initBtn()
    }
  }
}
