import { tns } from 'tiny-slider/src/tiny-slider'
import merge from 'lodash/merge'

export default class Carousel {
  constructor (ctx, cfg) {
    this.ctx = ctx

    if (!this.ctx) {
      throw new Error('No context specified')
    }

    this.track = this.ctx.querySelector('.js-carousel-track')
    this.controls = this.ctx.querySelector('.js-carousel-controls')

    this.initCfg(cfg)
  }

  initCfg (cfg) {
    const defaultCfg = {
      tns: {
        // default settings for tns
        controls: !!this.controls,
        controlsContainer: this.controls,
        navPosition: 'bottom'
      }
    }

    this.settings = merge(defaultCfg, cfg)
  }

  initCarousel () {
    if (!this.track) {
      throw new Error('No track specified')
    }

    this.carousel = tns({
      container: this.track,
      ...this.settings?.tns
    })
  }

  init () {
    this.initCarousel()
  }
}
