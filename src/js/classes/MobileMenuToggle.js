export default class {
  constructor (cfg) {
    this.menu = cfg?.menu
    this.btn = cfg?.btn
    this.isActive = false

    if (!this.menu) {
      throw new Error('Menu item not specified')
    }

    if (!this.btn) {
      throw new Error('Menu button/trigger not specified')
    }

    this.initCfg(cfg)
  }

  initCfg (cfg) {
    this.settings = {
      activeClass: 'is-active',
      ...cfg
    }
  }

  turnMenuOn () {
    this.btn.classList.add(this.settings.activeClass)
    this.menu.classList.add(this.settings.activeClass)
  }

  turnMenuOff () {
    this.btn.classList.remove(this.settings.activeClass)
    this.menu.classList.remove(this.settings.activeClass)
  }

  initBtnClick () {
    this.btn.addEventListener('click', () => {
      if (this.isActive) this.turnMenuOn()
      else this.turnMenuOff()
      this.isActive = !this.isActive
    })
  }

  init () {
    this.initBtnClick()
  }
}
