import Pristine from 'pristinejs'

export default class {
  constructor (form, cfg) {
    this.$form = form
    this.cfg = cfg
  }

  initValidator () {
    if (!this.$form) return

    this.validator = new Pristine(this.$form, this.cfg)
  }

  initFormSubmit () {
    if (!this.$form) return

    this.$form.addEventListener('submit', e => {
      e.preventDefault()

      const valid = this.validator.validate()

      if (valid) {
        // actions on form validation
      }
    })
  }

  init () {
    if (!this.$form) {
      throw new Error('Form element not specified')
    }

    this.initValidator()
    this.initFormSubmit()
  }
}
