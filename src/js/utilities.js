// Sizes, Breakpoints, Media
const BREAKPOINTS = {
  l: 1620,
  ls: 1280,
  t: 1200,
  tm: 980,
  m: 670,
  ms: 480,
  mt: 370
}

const MEDIA = {}

Object.keys(BREAKPOINTS).map(key => {
  MEDIA[key] = window.matchMedia(`(max-width: ${BREAKPOINTS[key]}px)`)
})

// Sliding animation
function slideDown (el) {
  el.style.maxHeight = '1000px'
  el.style.opacity = '1'
}

function slideUp (el) {
  el.style.maxHeight = '0'
  el.style.opacity = '0'
}

module.exports = {
  BREAKPOINTS,
  MEDIA,
  slideDown,
  slideUp
}
