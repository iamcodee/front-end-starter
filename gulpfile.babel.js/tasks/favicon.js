﻿import config from '../config'
import realFavicon from 'gulp-real-favicon'
import gulp from 'gulp'
import fs from 'fs'
import path from 'path'

var FAVICON_DATA_FILE = 'faviconData.json'
var FAVICON_TWIG = 'src/templates/head/favicon.twig'

const cfg = config.tasks.favicon
const src = path.posix.join(config.root.src, cfg.src)

gulp.task('generate-favicon', function (done) {
  realFavicon.generateFavicon({
    masterPicture: src,
    dest: cfg.dest,
    iconsPath: cfg.iconsPath,
    design: {
      ios: {
        pictureAspect: 'noChange',
        assets: {
          ios6AndPriorIcons: false,
          ios7AndLaterIcons: false,
          precomposedIcons: false,
          declareOnlyDefaultIcon: true
        }
      },
      desktopBrowser: {
        design: 'raw'
      },
      windows: {
        pictureAspect: 'noChange',
        backgroundColor: cfg.bgColor,
        onConflict: 'override',
        assets: {
          windows80Ie10Tile: false,
          windows10Ie11EdgeTiles: {
            small: false,
            medium: true,
            big: false,
            rectangle: false
          }
        }
      },
      androidChrome: {
        pictureAspect: 'noChange',
        themeColor: cfg.themeColor,
        manifest: {
          display: 'standalone',
          orientation: 'notSet',
          onConflict: 'override',
          declared: true
        },
        assets: {
          legacyIcon: false,
          lowResolutionIcons: false
        }
      },
      safariPinnedTab: {
        pictureAspect: 'silhouette',
        themeColor: cfg.themeColor
      }
    },
    settings: {
      scalingAlgorithm: 'Mitchell',
      errorOnImageTooSmall: false,
      readmeFile: false,
      htmlCodeFile: false,
      usePathAsIs: false
    },
    markupFile: FAVICON_DATA_FILE
  }, function () {
    done()
  })
})

gulp.task('inject-favicon-markups', function (done) {
  const str = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code
  fs.writeFileSync(FAVICON_TWIG, str)
  done()
})
