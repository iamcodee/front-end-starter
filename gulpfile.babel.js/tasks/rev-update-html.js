import { readFileSync } from 'fs'
import { src, dest } from 'gulp'
import revRewrite from 'gulp-rev-rewrite'
import config from '../config'
import path from 'path'

const revUpdateHTMLTask = () => {
  const finalPath = path.join(global.build ? config.root.dist : '', config.root.dest)
  const srcPath = global.build ? path.join('.', config.root.dist, '**/*.{css,html}') : './*.html'
  const destPath = global.build ? path.join(config.root.dist) : './'
  const manifest = readFileSync(path.join(finalPath, 'manifest.json'))

  return src(srcPath)
    .pipe(revRewrite({ manifest }))
    .pipe(dest(destPath))
}

export default revUpdateHTMLTask
