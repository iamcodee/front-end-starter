import { src, dest, lastRun } from 'gulp'
import path from 'path'
import config from '../config'

const favCfg = config.tasks.faviconToDist

const paths = {
  src: path.posix.join(config.root.base, favCfg.src, '/**/*'),
  dest: path.posix.join(config.root.dist, favCfg.dest)
}

const copyFaviconToDist = () => {
  return src(paths.src, { since: lastRun(copyFaviconToDist) })
    .pipe(dest(paths.dest))
}

export default copyFaviconToDist
