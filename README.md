# Front-end starter

**Front-end starter** - fast and simple web development starter.

___

## List of Content

1. [Features](#gift-features)
1. [Getting Started?](#getting-started)
    * [Recommendations](#recommendations)
    * [Static HTML templating](#static-html-templating)

## Features

|Features|Description|
|------------------|-----------|
|CSS| [SASS](http://sass-lang.com/), [Autoprefixer](https://github.com/postcss/autoprefixer), gulp-postcss, cssnano, critical |
|JS|[Webpack](https://webpack.js.org/), [Babel](http://babeljs.io/)|
|Live Reload|[BrowserSync](http://www.browsersync.io/), [Webpack Dev Middleware](https://github.com/webpack/webpack-dev-middleware), [Webpack Hot Middleware](https://github.com/glenjamin/webpack-hot-middleware)|
|HTML| gulp-twig, gulp-rev |
|Images| [gulp-imagemin](https://www.npmjs.com/package/gulp-imagemin)|
|SVG sprite| [gulp-svg-sprite](https://github.com/jkphl/gulp-svg-sprite)|
|Lint| standardjs |

## Getting started?

### Recommendations

Make sure you have installed the following: 

* [Node.js](https://nodejs.org/) (**Recommended to use Node.js >= v12.0.0**)
* [npm](https://www.npmjs.com/) or [yarn](https://yarnpkg.com/en/).
In this tutorial we use yarn, but you can use npm. 

## Static HTML templating

#### Step 1 - clone

```bash
git clone git@bitbucket.org:iamcodee/front-end-starter.git [my-project-name]
cd [my-project-name]
```

#### Step 2 - run

```bash
yarn
yarn dev
```

## Directory structure explanation

* **/** - root directory with html files and configuration files (eslint, editorconfig)
* **src** - directory with source files
* **static** - directory compiled files, do not edit files in this directory because they will be overwritten

## Commands

```bash
yarn dev - Runs development mode
yarn build - Compiles assets in production mode
yarn zip - Create a package of dist folder
yarn favicon - Generate favicons
yarn sprites - Generate Sprites SVG
```

## Config file

Config file is located in the "gulpfile.babel.js" folder.

#### Critical options

**enabled**

Type: Boolean

Enabled if is set true

#### Rev options

**enabled**

Type: Boolean

Enabled if is set true

#### Zip options

**prefix**

Type: String

Prefix of the package

**src**

Type: String

Path to "dist" folder

**dest**

Type: String

Path to "dest" folder

#### SVG Sprites options

**src**

Type: String

Path to location with icons in the folder "src"

**dest**

Type: String

Path to "dest" folder
